CREATE TABLE user (
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR (64) NOT NULL,
    password VARCHAR (128) NOT NULL,
    PRIMARY KEY (id)
) ENGINE = InnoDB CHARSET = utf8;