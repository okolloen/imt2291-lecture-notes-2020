<?php
require_once '../html/classes/DB.php';
require_once '../html/classes/User.php';

class AcceptanceCest {
  protected $user, $userData;

  public function _before(AcceptanceTester $I) {
    $db = DB::getDBConnection();
    $familyName = md5(date('l jS \of F Y h:i:s A'));  // Create random 32 character string
    $givenName = md5(date('l jS h:i:s A \of F Y '));  // Create random 32 character string
    $this->userData['givenName'] = $givenName;
    $this->userData['familyName'] = $familyName;
    $this->userData['uname'] = $givenName.'@'.$familyName.'.test';
    $this->userData['pwd'] = 'MittHemmeligePassord';

    $this->user = new User($db);
  }

  // tests
  public function firstPageExists(AcceptanceTester $I) {
    $I->amOnPage('/');
    $I->see('Dette er hovedsiden');
    $I->see('Ikke logget inn');
  }

  /**
   * Get default page, submit the login form, check that we get the
   * logged in page, then reload the page and check that we are still
   * logged in.
   */
  public function testCanLogIn(AcceptanceTester $I) {
    $userid = $this->user->addUser($this->userData)['id'];

    // Go to login page
    $I->amOnPage('/');
    $I->fillField('uname', $this->userData['uname']);
    $I->fillField('pwd', $this->userData['pwd']);
    $I->click('logg inn');

    $I->see('Hemmelig');
    $I->amOnPage('/');
    $I->see('Hemmelig');

    $this->user->deleteUser($userid);
  }

  /**
   * Test that we can log out, also that we stay logged out when
   * page is reloaded.
   */
  public function testLogout(AcceptanceTester $I) {
    $userid = $this->user->addUser($this->userData)['id'];

    // Go to login page
    $I->amOnPage('/');
    $I->fillField('uname', $this->userData['uname']);
    $I->fillField('pwd', $this->userData['pwd']);
    $I->click('logg inn');

    $I->click('logg ut');
    $I->see('Ikke logget inn');
    $I->amOnPage('/');
    $I->see('Ikke logget inn');
    $this->user->deleteUser($userid);
  }
}
