CREATE TABLE `fileStorage`.`filesInDB` (
    `id` BIGINT NOT NULL AUTO_INCREMENT,
    `owner` BIGINT NOT NULL,
    `name` VARCHAR(128) NOT NULL,
    `mime` VARCHAR(128) NOT NULL,
    `size` INT NOT NULL,
    `description` TEXT NOT NULL,
    `content` LONGBLOB NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;