<?php
require_once '../../vendor/autoload.php';
require_once '../classes/DB.php';

$loader = new \Twig\Loader\FilesystemLoader('./twig');
$twig = new \Twig\Environment($loader, [
    /* 'cache' => './compilation_cache', // Only enable cache when everything works correctly */
]);

$db = DB::getDBConnection();

$sql = 'SELECT id, owner, name, size, description FROM filesOnDisc ORDER BY name';
$sth = $db->prepare ($sql);
$sth->execute();

$result = $sth->fetchAll(PDO::FETCH_ASSOC);

echo $twig->render('index.html', array ('files'=>$result));
